
module Var where

import Control.Monad.Trans.Class

import Data.IORef

class Monad m => HasVar m where
  type Var m :: * -> *
  get  :: Var m a -> m a
  ($=) :: Var m a -> a -> m ()
  new  :: a -> m (Var m a)

instance HasVar IO where
  type Var IO = IORef
  get  = readIORef
  ($=) = writeIORef
  new  = newIORef

instance (MonadTrans t, HasVar m, Monad (t m)) => HasVar (t m) where
  type Var (t m) = Var m
  get  =  lift . get
  ($=) = (lift .) . ($=)
  new  =  lift . new