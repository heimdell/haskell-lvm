
module LVM where

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.Base
import Control.Monad.Trans.Control
import Control.Concurrent.Lifted
import Data.IORef
import Data.Traversable (for)

import Var
import Program
import Literal
import qualified Env
import qualified Name

data Value
  = BIF  [Node] Builtin
  | Ctor [Node] (Name.T, Int)
  | Lit Literal
  | Obj Context
  | Fun Context Name.T Program
  | Pzm Prizm

instance Show Value where
  show = \case
    BIF  {}          -> "<builtin>"
    Ctor _ (name, n) -> name ++ "@" ++ show n
    Lit  l           -> show l
    Obj  ctx         -> "{" ++ unwords (Env.keys ctx) ++ "}"
    Fun  _ arg body  -> "\\" ++ arg ++ " -> " ++ show body
    Pzm  p           -> show p


type Builtin = ([Node] -> M Node, Int)
type Context = Env.T Name.T Node
type Node    = IORef (Either (Closed Program) Value)

data Closed a = Closed
  { context :: Maybe Context
  , program :: a
  }

newtype M a = M { unM :: ExceptT Error (ReaderT Context IO) a }
  deriving newtype
    ( Functor
    , Applicative
    , Monad
    , MonadError Error
    , MonadReader Context
    , MonadFix
    , MonadBase IO
    , MonadBaseControl IO
    , HasVar
    )

data Error
  = Undefined [Name.T]
  | Expected Type_
  | At String Error
  deriving stock (Show)

notDefined :: Name.T -> M a
notDefined = throwError . Undefined . pure

allNotDefined :: [Name.T] -> M a
allNotDefined = throwError . Undefined

expected :: Type_ -> M a
expected = throwError . Expected

data Type_
  = Func
  | Atom String
  | Construct
  | Object
  | Prizm
  deriving stock (Show)

runM :: M a -> IO (Either Error a)
runM (M ma) = runReaderT (runExceptT ma) mempty

whnf :: Node -> M Value
whnf node = do
  get node >>= \case
    Left (Closed mCtx prog) -> do
      val <- case mCtx of
        Just ctx -> do
          local (const ctx) do
            eval prog
        Nothing -> do
          eval prog

      node $= Right val
      return val

    Right val -> do
      return val

lazy :: Program -> M Node
lazy = new . Left . Closed Nothing

close :: Context -> Program -> M Node
close = ((new . Left) .) . Closed . Just

eval :: Program -> M Value
eval = \case
  Var name -> do
    asks (Env.lookup name) >>= \case
      Just it -> whnf it
      Nothing -> notDefined name

  App f x -> do
    x' <- lazy x
    eval f >>= \case
      Ctor xs (p, n) | n > 0 -> return $ Ctor (x' : xs) (p, n - 1)
      BIF  xs (g, n) | n > 1 -> return $ BIF  (x' : xs) (g, n - 1)
      BIF  xs (g, _)         -> g (reverse (x' : xs)) >>= whnf

      Pzm (IsTag s n) | n > 1 ->
        return $ Ctor [x'] (s, n - 1)

      Fun ctx arg body -> do
        local (const (Env.single arg x' <> ctx)) do
          eval body

      _ -> expected Func

  Let ctx body -> mdo
    let env = Env.fromList ctx
    frame <- for env $ close frame

    local (frame <>) do
      eval body

  Lam arg body -> do
    env <- ask
    return $ Fun env arg body

  IfMatch prism subj (ns, y) n -> do
    eval prism >>= \case
      Pzm p -> do
        val <- eval subj
        matches p val >>= \case
          Just parts -> do
            local (Env.fromZip ns parts <>) do
              eval y

          Nothing -> do
            eval n

      _ -> do
        expected LVM.Prizm

  Program.Prizm prism -> do
    return $ Pzm prism

  New names -> do
    env <- ask
    case Env.lookupAll names env of
      Just vals -> do
        return $ Obj $ Env.fromZip names vals

      Nothing -> do
        allNotDefined $ Env.unknown names env

  Literal l -> do
    return $ Lit l

  Seq a b -> do
    void $ eval a
    eval b

  Par a b -> do
    void $ fork do
      void $ eval a
    eval b

matches :: Prizm -> Value -> M (Maybe [Node])
matches prism val = case (prism, val) of
  (IsTag p _, Ctor nodes (p', 0)) -> do
    return $
      if p == p'
      then Just nodes
      else Nothing

  (IsTag {}, _) -> do
    expected Construct

  (IsObj names, Obj ctx) -> do
    return $ Env.lookupAll names ctx

  (IsObj {}, _) -> do
    expected Object

  (IsLit lit, Lit lit') -> do
    return $
      if lit == lit'
      then Just []
      else Nothing

  (IsLit _, _) -> do
    expected (Atom "any")

test :: IO (Either Error Value)
test = runM (eval prog)
  where
    prog =
      Let
        [ ("fix",  Lam "f" (App #f (App #fix #f)))
        , ("cons", Program.Prizm (IsTag "cons" 2))
        , ("ones", App #fix (App #cons 1))
        ]
        #ones
