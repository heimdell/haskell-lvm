
module Program where

import Data.List (intercalate)
import Data.String
import Data.Proxy

import GHC.OverloadedLabels
import GHC.TypeLits

import qualified Name
import Literal

data Program
  = Var Name.T
  | Bif String
  | App Program Program
  | Let [(Name.T, Program)] Program
  | Lam Name.T Program
  | New [Name.T]
  | Prizm Prizm
  | Literal Literal
  | Seq Program Program
  | Par Program Program
  | IfMatch
      { prizm   :: Program
      , subject :: Program
      , yes     :: ([Name.T], Program)
      , no      :: Program
      }

data Prizm
  = IsTag Name.T Int
  | IsObj [Name.T]
  | IsLit Literal

instance Num Program where
  fromInteger = Literal . fromInteger

instance IsString Program where
  fromString = Literal . fromString

instance KnownSymbol label => IsLabel label Program where
  fromLabel = Var (symbolVal (Proxy :: Proxy label))

instance Show Program where
  show = \case
    Var n     -> n
    Bif n     -> "%" ++ n
    App f   x -> "(" ++ show f ++ " " ++ show x ++ ")"
    Let ctx b -> "let " ++ intercalate "; " (map (\(n, decl) -> n ++ " = " ++ show decl) ctx) ++ " in " ++ show b
    Lam a   b -> "\\" ++ a ++ " -> " ++ show b
    New names -> "{" ++ unwords names ++ "}"
    Seq a   b -> "(seq " ++ show a ++ " " ++ show b ++ ")"
    Par a   b -> "(par " ++ show a ++ " " ++ show b ++ ")"

    Prizm   p -> show p
    Literal l -> show l

    IfMatch p s (ns, y) n -> "(match " ++ show p ++ " (" ++ show s ++ ") " ++ unwords ns ++ " -> " ++ show y ++ " : " ++ show n ++ ")"

instance Show Prizm where
  show = \case
    IsTag n s -> "#" ++ n ++ "/" ++ show s
    IsObj ns  -> "#{" ++ unwords ns ++ "}"
    IsLit l   -> "#" ++ show l