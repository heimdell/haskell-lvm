
module LVM1 where

-- import Control.Monad

import qualified Data.Map as Map
import Data.Functor (($>))
import Data.Traversable (for)

import Polysemy
import Polysemy.Error
import Polysemy.Reader
import Polysemy.State
import Polysemy.Fixpoint

import Literal
import qualified Name
import Program
import qualified Env

data Value
  = BIF  [NodeId] Builtin
  | Ctor [NodeId] (Name.T, Int)
  | Lit  Literal
  | Obj  Context
  | Fun  Context Name.T Program
  | Pzm  Prizm

instance Show Value where
  show = \case
    BIF  {}          -> "<builtin>"
    Ctor _ (name, n) -> name ++ "@" ++ show n
    Lit  l           -> show l
    Obj  ctx         -> "{" ++ unwords (Env.keys ctx) ++ "}"
    Fun  _ arg body  -> "\\" ++ arg ++ " -> " ++ show body
    Pzm  p           -> show p


type Builtin = (Name.T, Int)
type Context = Env.T Name.T NodeId
type Node    = Either (Closed Program) Value
type NodeId  = Int

data Closed a = Closed
  { context :: Maybe Context
  , program :: a
  }
  deriving Show

data Stdlib (m :: * -> *) a where
  Invoke :: String -> [Node] -> Stdlib m Node
  Ref    :: String           -> Stdlib m Builtin

makeSem ''Stdlib

data NotFound = NotFound [String]
  deriving Show

runStdlib
  :: Member (Error NotFound) r
  => Map.Map String (Int, [Node] -> Sem r Node)
  -> Sem (Stdlib : r) a
  -> Sem r a
runStdlib lib = interpret \case
  Invoke name args -> do
    let scan = Map.lookup name lib
    case scan of
      Just (_, f) -> f args
      Nothing -> throw (NotFound [name])

  Ref name -> do
    let scan = Map.lookup name lib
    case scan of
      Just (n, _) -> return (name, n)
      Nothing -> throw (NotFound [name])

data HasContext m a where
  Find       :: Name.T         -> HasContext m NodeId
  Capture    ::                   HasContext m Context
  WithFrame  :: Context -> m a -> HasContext m a
  UsingFrame :: Context -> m a -> HasContext m a

makeSem ''HasContext

runHasContextViaReader
  :: Members [Error NotFound, Reader Context] r
  => Sem (HasContext : r) a
  -> Sem r a
runHasContextViaReader = interpretH \case
  Find name -> do
    env <- ask @Context
    case Env.lookup name env of
      Just it -> pureT it
      Nothing -> throw (NotFound [name])

  WithFrame ctx ma -> do
    ma' <- runT ma
    local (ctx <>) do
      raise $ runHasContextViaReader ma'

  UsingFrame ctx ma -> do
    ma' <- runT ma
    local (const ctx) do
      raise $ runHasContextViaReader ma'

  Capture -> do
    ask @Context >>= pureT

data HasMemory (m :: * -> *) a where
  Alloc  :: Node           -> HasMemory m NodeId
  Access :: NodeId         -> HasMemory m Node
  Update :: NodeId -> Node -> HasMemory m ()

makeSem ''HasMemory

data MemoryCorruption = MemoryCorruption NodeId
  deriving Show

runHasMemoryViaState
  :: Members [State (Map.Map NodeId Node), Error MemoryCorruption] r
  => Sem (HasMemory : r) a
  -> Sem r a
runHasMemoryViaState = interpret \case
  Alloc node -> do
    ks <- gets Map.keys
    let
      n = case ks of
        []    -> 0
        k : _ -> k
    modify (Map.insert (n - 1) node)
    return (n - 1)

  Access nid -> do
    scan <- gets (Map.lookup nid)
    case scan of
      Just it -> return it
      Nothing -> throw (MemoryCorruption nid)

  Update nid node -> do
    modify (Map.insert nid node)

data Parallel m a where
  Parallel :: m a -> Parallel m ()

makeSem ''Parallel

runParallelButNotReally
  :: forall e a r. Sem (Parallel : r) a -> Sem r a
runParallelButNotReally = interpretH \case
  Parallel ma -> do
    it <- runT ma
    raise do
      fa <- runParallelButNotReally it
      return (fa $> ())

whnf
  :: Members
    [ HasContext
    , HasMemory
    , Stdlib
    , Error NotFound
    , Error Expected
    , Fixpoint
    , Parallel
    ] r
  => NodeId -> Sem r Value
whnf nid = do
  node <- access nid
  case node of
    Left (Closed ctx prog) -> do
      val <- maybe id usingFrame ctx do
        eval prog

      update nid (Right val)
      return val

    Right val -> do
      return val

delay :: Members '[HasMemory] r => Program -> Sem r NodeId
delay prog = do
  alloc (Left (Closed Nothing prog))

close :: Members '[HasMemory] r => Context -> Program -> Sem r NodeId
close = ((alloc . Left) .) . Closed . Just

data Expected
  = ExpectedFunc
  | ExpectedPrizm
  | ExpectedConstruct
  | ExpectedObject
  | ExpectedAtomic
  deriving Show

eval
  :: Members
    [ HasContext
    , HasMemory
    , Stdlib
    , Error NotFound
    , Error Expected
    , Fixpoint
    , Parallel
    ] r
  => Program -> Sem r Value
eval = \case
  Var name -> do
    find name >>= whnf

  App f x -> do
    x' <- delay x
    eval f >>= \case
      Ctor xs (p, n) | n > 0 -> return $ Ctor (x' : xs) (p, n - 1)
      BIF  xs (g, n) | n > 1 -> return $ BIF  (x' : xs) (g, n - 1)
      BIF  xs (g, _)         -> do
        nodes <- for (x' : xs) access
        invoke g (reverse nodes) >>= alloc >>= whnf

      Pzm (IsTag s n) | n > 1 ->
        return $ Ctor [x'] (s, n - 1)

      Fun ctx arg body -> do
        usingFrame (Env.single arg x' <> ctx) do
          eval body

      _ -> throw ExpectedFunc

  Let preCtx body -> mdo
    let env = Env.fromList preCtx
    frame <- for env $ close frame

    withFrame frame do
      eval body

  Lam arg body -> do
    env <- capture
    return $ Fun env arg body

  New names -> do
    vals <- for names find
    return $ Obj $ Env.fromZip names vals

  Bif n -> do
    builtin <- ref n
    return $ BIF [] builtin

  Prizm p -> do
    return $ Pzm p

  Literal l -> do
    return $ Lit l

  Seq a b -> do
    _ <- eval a
    eval b

  Par a b -> do
    parallel do
      eval a
    eval b

  IfMatch prism subj (ns, y) n -> do
    eval prism >>= \case
      Pzm p -> do
        val <- eval subj
        matches p val >>= \case
          Just parts -> do
            withFrame (Env.fromZip ns parts) do
              eval y

          Nothing -> do
            eval n

      _ -> do
        throw ExpectedPrizm

matches
  :: Members '[Error Expected] r
  => Prizm
  -> Value
  -> Sem r (Maybe [NodeId])
matches prism val = case (prism, val) of
  (IsTag p _,   Ctor nodes (p', 0)) -> return if p   == p'   then Just nodes else Nothing
  (IsLit lit,   Lit  lit')          -> return if lit == lit' then Just []    else Nothing
  (IsObj names, Obj  ctx)           -> return $ Env.lookupAll names ctx

  (IsLit _,  _) -> throw ExpectedAtomic
  (IsObj {}, _) -> throw ExpectedObject
  (IsTag {}, _) -> throw ExpectedConstruct

data Union xs where
  Here  ::       x  -> Union (x : xs)
  There :: Union xs -> Union (x : xs)

instance Show (Union '[]) where
  show = \case

instance (Show (Union xs), Show x) => Show (Union (x : xs)) where
  show = \case
    Here  e -> show e
    There e -> show e

class OneOf x xs where
  inject  :: x -> Union xs
  project :: Union xs -> Maybe x

instance {-# OVERLAPS #-} OneOf x (x : xs) where
  inject = Here
  project (Here e) = Just e
  project  _       = Nothing

instance {-# OVERLAPPABLE #-} OneOf y xs => OneOf y (x : xs)  where
  inject = There . inject
  project (There e) = project e
  project  _        = Nothing

addError
  :: forall err errs r a
  .  ( Member (Error (Union errs)) r
     , OneOf err errs
     )
  => Sem (Error err : r) a
  -> Sem r a
addError ma = do
  mea <- runError ma
  either (throw . inject) return mea

newtype BuiltinFunction = BuiltinFunction
  { unwrap :: forall r. Members '[HasMemory] r => [Node] -> Sem r Node
  }

type AllErrors = Union [Expected, NotFound, MemoryCorruption]

evalPure
  :: Program
  -> Map.Map String (Int, BuiltinFunction)
  -> (Map.Map NodeId Node, Either AllErrors Value)
evalPure prog lib =
  run
  $ runFixpoint run
  $ runParallelButNotReally
  $ runError
  $ addError @Expected
  $ addError @NotFound
  $ addError @MemoryCorruption
  $ runReader Env.empty
  $ runState  Map.empty
  $ runHasContextViaReader
  $ runHasMemoryViaState
  $ runStdlib ((fmap.fmap) unwrap lib)
  $ eval prog
