
module Env where

import Data.Maybe
import qualified Data.Map as Map
import Data.Traversable (for)

newtype T k v = Env { getEnv :: Map.Map k v }
  deriving newtype (Show, Semigroup, Monoid, Functor, Foldable)

instance Traversable (T k) where
  traverse action (Env m) = Env <$> traverse action m

empty :: T k v
empty = Env Map.empty

fromList :: Ord k => [(k, v)] -> T k v
fromList = Env . Map.fromList

fromZip :: Ord k => [k] -> [v] -> T k v
fromZip = (fromList .) . zip

toList :: T k v -> [(k, v)]
toList = Map.toList . getEnv

single :: k -> v -> T k v
single = (Env .) . Map.singleton

lookup :: Ord k => k -> T k v -> Maybe v
lookup = (. getEnv) . Map.lookup

map :: (v -> v') -> T k v -> T k v'
map f = Env . Map.map f . getEnv

lookupAll :: Ord k => [k] -> T k v -> Maybe [v]
lookupAll ks env = for ks (`Env.lookup` env)

unknown :: Ord k => [k] -> T k v -> [k]
unknown ks e = filter (isNothing . (`Env.lookup` e)) ks

keys :: T k v -> [k]
keys = Map.keys . getEnv
