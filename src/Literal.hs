
module Literal where

import Data.String

data Literal
  = S String
  | F Double
  | I Integer
  deriving (Eq)

instance Num Literal where
  fromInteger = I

instance IsString Literal where
  fromString = S

instance Show Literal where
  show = \case
    I i -> show i
    F i -> show i
    S i -> show i